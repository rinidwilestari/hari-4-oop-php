<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("Shaun");
echo "Name : " .$sheep ->name ."<br>";
echo "legs : " .$sheep ->legs ."<br>";
echo "Cold Blooded : " .$sheep ->cold_bold ."<br><br>";

$frog = new Frog("Buduk");
echo "Name : " .$frog ->name ."<br>";
echo "legs : " .$frog ->legs ."<br>";
echo "Cold Blooded :" .$frog ->cold_bold ."<br>";
echo "Jump : " .$frog ->jump ."<br><br>";

$ape = new Ape("Kera Sakti");
echo "Name : " .$ape ->name ."<br>";
echo "legs : " .$ape ->legs ."<br>";
echo "Cold Blooded :" .$ape ->cold_bold ."<br>";
echo "Yell : " .$ape ->yell ."<br><br>";
?>